import React from "react";
import { Route, Switch } from "react-router-dom";

import About from "./components/pages/About";
import Home from "./components/pages/Home";

const AppRouter = () => {
  return (
    <Switch>
      <Route exact path="/" component={Home} />
      <Route exact path="/about" component={About} />
    </Switch>
  );
};

export default AppRouter;
