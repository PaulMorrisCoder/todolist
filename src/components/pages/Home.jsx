import React, { Component, Fragment } from "react";

import Todos from "../../components/Todos";
import AddTodo from "../../components/AddTodo";

class Home extends Component {
  render() {
    return (
      <Fragment>
        <AddTodo addTodo={this.addTodo} />
        <Todos
          todos={this.state.todos}
          markComplete={this.markComplete}
          delTodo={this.delTodo}
        />
      </Fragment>
    );
  }
}

export default Home;
