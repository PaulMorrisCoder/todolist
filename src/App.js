import React, { Component, Fragment } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";

import uuid from "uuid";

import Todos from "./components/Todos";
import Header from "./components/layout/Header";
import AddTodo from "./components//AddTodo";

import AppRouter from "./AppRouter";

import About from "./components/pages/About";
import axios from "axios";

class App extends Component {
  state = {
    todos: []
  };

  componentDidMount() {
    axios
      .get("https://jsonplaceholder.typicode.com/todos?_limit=10")
      .then(response => this.setState({ todos: response.data }));
  }
  markComplete = id => {
    this.setState({
      todos: this.state.todos.map(todo => {
        if (todo.id === id) {
          todo.completed = !todo.completed;
        }
        return todo;
      })
    });
  };

  delTodo = id => {
    axios
      .delete(`https://jsonplaceholder.typicode.com/todos/${id}`)
      .then(response =>
        this.setState({
          todos: [...this.state.todos.filter(todo => todo.id !== id)]
        })
      );
  };

  addTodo = title => {
    axios
      .post("https://jsonplaceholder.typicode.com/todos", {
        title,
        completed: false
      })
      .then(response => {
        console.log(response);
        const item = { ...response.data, id: uuid.v4() };
        console.log(item);
        this.setState({ todos: [...this.state.todos, item] });
      });
  };

  render() {
    return (
      <AppRouter>
        <div className="App">
          <div className="container">
            <Header />
            {/* <Route
              exact
              path="/"
              render={props => (
                <Fragment>
                  <AddTodo addTodo={this.addTodo} />
                  <Todos
                    todos={this.state.todos}
                    markComplete={this.markComplete}
                    delTodo={this.delTodo}
                  />
                </Fragment>
              )}
            />
            <Route path="/about" component={About} /> */}
          </div>
        </div>
      </AppRouter>
    );
  }
}

export default App;
